### Simulation-based verification of MCS protocol
- `label-pid.cafe`: defines LABEL, PID modules.
- `mcs.cafe`: original version of MCS protocol.
- `mcs2.cafe`: made by only adding observation topQ into original version.
- `mcs3.cafe`: made by replacing two conditions in MCS2 with new conditions base on topQ.
- `proof_score` directory: proof scores of MCS3.
- `simulation` directory: 
  * `simulation_1_2.cafe`: define the simulation between MCS and MCS2.
  * `simulation_2_3.cafe`: define the simulation between MCS2 and MCS3.
  * `simulation_1_2_proof.cafe`: proof of the simulation between MCS and MCS2.
  * `simulation_2_3_proof.cafe`: proof of the simulation between MCS2 and MCS3.
  * `simulation_1_3.cafe`: define the simulation between MCS and MCS3.
  * `mcs3_implies_mcs.cafe`: proof mutex of MCS using simulation and mutex of MCS3.